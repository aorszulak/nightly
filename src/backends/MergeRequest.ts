export interface IMergeRequest {
	title: string;
	branch: string;
}
