export class Options {
	public username: string;
	public password: string;
	public repo: string;
	public owner: string;
	public directory: string;
	public include?: RegExp;
	public exclude?: RegExp;
	public dry: boolean;

	constructor({
		directory,
		owner,
		password,
		repo,
		username,
		include,
		exclude,
		dry,
	}: {
		directory: string;
		owner: string;
		password: string;
		repo: string;
		username: string;
		include?: string;
		exclude?: string;
		dry?: boolean;
	}) {
		this.directory = directory;
		this.owner = owner;
		this.password = password;
		this.repo = repo;
		this.username = username;
		if (include) {
			this.include = new RegExp(include);
		}
		if (exclude) {
			this.exclude = new RegExp(exclude);
		}
		this.dry = !!dry;
	}
}
